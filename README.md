# linux-vmware-host-modules-builder-cli
This shell script downloads, builds and installs `Vmware` host modules for your Linux `Vmware`.

The script will download the required `vmmnet` and `vmmon` for your `Vmware` version then build and install them for you.


</br>

# Supported Linux systems
  1. Debian based distros (Tested on Debian, Ubuntu, Pop, Kali)
  2. Arch Linux.
  3. **Coming soon** : Fedora Workstation.


</br>

# Download
To download and use this script:
  1. Click on *`code`* on this page.
  2. Click on *`Download Zip`*.
  3. A files named *`vmware-host-modules-builder-cli-master.zip`* will be downloaded. Extract the zip file in your computer. You will see a folder labelled *`vmware-host-modules-builder-cli-master`*. Navigate to that folder.
  4. Open terminal at the files location by right clicking and selecting *`Open in terminal`*. You can as well launch your terminal and navigate to where the downloaded file is.

  5. For Debian, type **`chmod +x debian-vmware-host-modules-builder-cli.sh`** and click on `enter key` to make the script executable.

  6. For Arch Linux, type **`chmod +x archlinux-vmware-host-modules-builder-cli.sh`** and click on `enter key` to make the script executable.

  6. Type **`./linux-vmware-host-modules-builder-cli.sh`** to run the script.

</br></br>
>You also, can download the host modules and build them manually from Michal Kubeček(https://github.com/mkubecek/vmware-host-modules)
